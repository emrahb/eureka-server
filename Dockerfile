FROM openjdk:8
ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/share/eureka-server/eureka-server.jar"]

ADD target/eureka-server.jar /usr/share/eureka-server/eureka-server.jar